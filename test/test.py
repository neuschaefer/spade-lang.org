import asyncio

from playwright.async_api import async_playwright, Page, Playwright, ViewportSize

URL = "http://localhost:1111"
# TODO: use real heights
VIEWPORTS: dict[str, ViewportSize] = {
    "tiny": {"width": 320, "height": 600},
    "sm": {"width": 640, "height": 1000},
    "md": {"width": 768, "height": 1000},

    "lg": {"width": 1024, "height": 700},
    "xl": {"width": 1280, "height": 800},
    "2xl": {"width": 1536, "height": 900},
    "3xl": {"width": 1700, "height": 1000},
}

PAGES = {
    "": ["spade", "an-overview-of-the-language", "publications"],
    "showcase": ["older-projects"],
}


async def _take_screenshots(sem, context, slug, headers, viewport_size, index, key):
    url = f"{URL}/{slug}"
    page: Page = await context.new_page()
    await page.set_viewport_size(viewport_size)

    header_index = 0
    def _path(header_key):
        nonlocal header_index
        header_index += 1
        return f"screenshots/{index}-{header_index}_{key}-{header_key}.png"

    await page.goto(url)
    await page.screenshot(path=_path("initial"))
    sem.release()

    async def _header(header):
        await page.goto(f"{url}/#{header}")
        await page.screenshot(path=_path(header))
        sem.release()

    for header in headers:
        await _header(header)


async def main(playwright: Playwright):
    total = len(VIEWPORTS) * sum(((1 + len(headers)) for headers in PAGES.values()))
    print(f"found {total} screenshots to take")

    # This sema is released (increased) every time a screenshot is taken.
    done_sem = asyncio.Semaphore(value=0)
    async def _handle_done():
        done = 0
        while done != total:
            await done_sem.acquire()
            done += 1
            print(".", end="", flush=True)
            if (done % 50 == 0) and done != total:
                print("")
            elif (done % 10 == 0) and done != total:
                print(" ", end="", flush=True)
        print("")


    print("setting up browser... ", end="", flush=True)
    browser = await playwright.firefox.launch()
    context = await browser.new_context()
    print("done")
    _handle_done_task = asyncio.create_task(_handle_done())
    async with asyncio.TaskGroup() as tg:
        for i, (url, headers) in enumerate(PAGES.items(), start=1):
            for j, (vp_key, vp_size) in enumerate(VIEWPORTS.items(), start=1):
                tg.create_task(_take_screenshots(done_sem, context, url, headers, vp_size, f"{i}-{j}", f"{url if url else 'root'}-{vp_key}"))
    await browser.close()


if __name__ == "__main__":
    async def _run():
        async with async_playwright() as playwright:
            await main(playwright)

    asyncio.run(_run())
