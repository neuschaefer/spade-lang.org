To use this website as a Zola theme you will also need to setup tailwindcss.
This file will walk through the steps required to get a new website up and
running.

## Dependencies

- npm: https://www.npmjs.com/
- zola: https://www.getzola.org

## Setup repository and install build tools

```sh
$ zola init new-spade-site
# ...
> What is the URL of your site? (https://example.com): <your url>
> Do you want to enable Sass compilation? [Y/n]: n
> Do you want to enable syntax highlighting? [y/N]: y
> Do you want to build a search index of the content? [y/N]: n
# ...
$ cd new-spade-site
$ git init
$ git submodule add https://gitlab.com/spade-lang/spade-lang.org themes/spade
$ cp -r themes/spade/{styles,package.json,tailwind.config.js,.prettierrc} .
$ npm install
```

Open up `tailwind.config.js` and change `module.exports.content` to

```js
  content: ["./templates/**/*.html", "./themes/spade/templates/**/*.html"]
```

Next, **add** the following to `config.toml`:

```toml
theme = "spade"

[markdown]
highlight_code = true # this should alredy be here
highlight_theme = "ayu-mirage" # or a theme of your choice
extra_syntaxes_and_themes = ["themes/spade/syntaxes"] # for highlighting spade code

[extra]
toc = true # if you want

# these are shown in the top left
logo_name = "Site name"
logo_path = "spade.svg"

# these are shown in the top right
extra_menu = [
  { title = "Spade", link = "https://spade-lang.org", weight = 1 },
  { title = "Gitlab", link = "https://gitlab.com/spade-lang/spade", icon = "/gitlab-color.svg" },
  { title = "Discord community", link = "https://discord.gg/YtXbeamxEX", icon = "/discord-white.svg" },
]

# This isn't used currently, but it's good to have.
repository_url = "https://gitlab.com/spade-lang/spade"
# This is shown in the footer
site_repository_url = "https://gitlab.com/spade-lang/blog.spade-lang.org/"
```

Finally, create `content/_index.md` and write your landing page!

```markdown
---
template: section.html
---

hello world!! :D
```

## Developing

After making changes, you want to rebuild the css and the site.

```sh
$ npx tailwindcss -i styles/styles.css -o static/styles/styles.css
$ zola build
$ # start a http server, e.g.:
$ python -m http.server -d public
```

I recommend using a multiplexer to run tailwind and zola in the background. The commands you'll want to run are

- `$ npx tailwindcss -i styles/styles.css -o static/styles/styles.css --watch`
- `$ zola serve`

I like to use https://github.com/pvolok/mprocs for this.

## Tips and tricks

Tailwind needs to know which HTML-files are used so it can add the used utility
classes to the css file. The default `tailwind.config.js` points to
`themes/spade/**/*.html` and `templates/**/*.html` which works pretty good most
of the time. Sadly it won't work for HTML written directly in the `.md`-files
and if the utility class is written as a macro, e.g. `class="w-{{width}}"`. In
these cases you can write the classes manually in the tailwind
`module.exports.safelist`.

## Publishing

Feel free to use the `.gitlab-ci.yml` from the `spade-lang.org`-repository. It
installs tailwind and zola and publishes to GitLab pages. If you want to use
another hosting provider, you can still use the script-part to build the site
and then publish some other way.
