+++
title = "Project Showcase"
weight = 0
template = "page.html"
[extra]
updated = "2023-03-01"
+++

This is a listing of some projects built in Spade, intended for inspiration and
to get a feel for how the language looks and behaves.

## Maintained projects

The following are projects which are in active use and development, and therefore
usually keep up with changes to the language.

### Spade-V

A five stage RISC-V pipeline implementation. Currently intended to support the
base RV32 instruction set, but might be extended down the line.


- [https://gitlab.com/TheZoq2/spade-v](https://gitlab.com/TheZoq2/spade-v)

### DP-Executor

Hardware for running dynamic programming to optimize the fuel use of hybrid
electric vehicles. Paired with a fully pipelined vehicle model, this project
schedules the execution of the model across a configurable number of model
evaluation units, and manages data reads and writes between units.



- [https://gitlab.com/TheZoq2/dp-executor](https://gitlab.com/TheZoq2/dp-executor)

### Birbstar

Interfaces with two ADCs to run auto-correlation on two audio signals in order
to compute where sounds are coming from. Useful for finding birds.

- [https://gitlab.com/TheZoq2/birbstar/](https://gitlab.com/TheZoq2/birbstar/)


## Older projects

These are projects which are no longer actively maintained, meaning that while they
might not reflect the language in its current state, they are still good showcases
of its capabilities.

### snoωo

A snowboarding game built by 4 people in 48 hours during a game jam in the fall
of 2022. `1366x768@30Hz` HDMI output on an ECP5 FPGA. The whole game is implemented
in hardware, meaning there is no softcore processor anywhere in the project.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/OyvWaslF0NA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- [https://gitlab.com/sornas/snoo](https://gitlab.com/sornas/snoo)

### Tiny Tapeout FPGA

A small FPGA with 12 2-input LUTs, submitted to [tiny tapeout](https://tinytapeout.com/).
Makes heavy use of the ports feature for the configuration chain.

A video showing it running on the ulx3s board. Initially it's loaded with
independent 2 and gates, then I reconfigure it as a 4 way OR.
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/b3i51Nx6yXI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

- [https://gitlab.com/TheZoq2/tinytapeout02](https://gitlab.com/TheZoq2/tinytapeout02)


### Miscellaneous libraries

A collection of libraries used by other Spade projects, primarily for communicating with
external hardware. Sometimes updated to newer Spade versions when the need arrises.

- [HDMI for the ulx3s board](https://gitlab.com/TheZoq2/ulx3s_hdmi_spade)
- [Sharp memory display driver](https://gitlab.com/TheZoq2/spade-memory-display)
- [ws2812 RGB led driver](https://gitlab.com/TheZoq2/ws2812-spade)
    - Has documentation of how it was implemented at [https://docs.spade-lang.org/ws2812-example/](https://docs.spade-lang.org/ws2812-example/)
- [UART transmit and receive](https://gitlab.com/TheZoq2/spade_protocols)
- [mcp3002 ADC driver](https://gitlab.com/TheZoq2/mcp3002-spade)
