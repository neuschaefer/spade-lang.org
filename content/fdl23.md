+++
title = "Enhancing Compiler-Driven HDL Design with Automatic Waveform Analysis"
weight = 1
+++

*Paper resources*
- [Example project](https://gitlab.com/TheZoq2/remora-code/)
- [Swim plugin](https://gitlab.com/spade-lang/wal_analysis)
- [Paper](../fdl2023.pdf)
- [Slides](../fdl2023-presentation.pdf)

*Project links*
- [WAL website](wal-lang.org)
- [Spade website](spade-lang.org)

*Contact*

- [lucas.klemmer@jku.at](mailto:lucas.klemmer@jku.at)
- [frans.skarman@liu.se](mailto:frans.skarman@liu.se)

